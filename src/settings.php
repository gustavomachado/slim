<?php

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' => false,
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],
        'db' => [
            'driver' => 'sqlsrv',
            'host' => '172.16.1.133',
             'database' => 'sberp',
            'username' => 'sa',
            'password' => 'q21mdftase07d07',
//            'schema'=>'base',
        //    'charset' => 'utf8',
         //   'collation' => 'utf8_unicode_ci',
        //    'prefix' => 'tb_'
        ],
//        'db'=>[
//            'driver'=>'mysql',
//            'host'=>'172.16.93.23',
//            'database'=>'erfood',
//            'username'=>'gustavo',
//            'password'=>'sbti#gsm321',
//            'charset'=>'utf8',
//            'collation'=>'utf8_unicode_ci',
//            'prefix'=>'tbl_'
//        ],
        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
    ],
];
