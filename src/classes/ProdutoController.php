<?php

namespace App;

//use App\Controller as Controller;
use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ProdutoController {

    protected $table;

    public function __construct(Builder $table) {
        $this->table = $table;
//        $this->table
//                        ->join('produto_setor', 'produto_setor.id_produto', '=', 'produto.id_produto')
//                ->columns = [
//            "produto.id_produto",
//            "tx_produto",
//            "produto_setor.nb_preco_unitario"
//        ];
    }

    public function listAll(Request $request, Response $response, $args) {
        $produtos = $this->table->selectRaw(" * ",[])->get();
        var_dump($produtos);exit;
        return $response->getBody()->write(json_encode($produtos));
    }

    public function listByName(Request $request, Response $response, $args) {
        $name = $args['name'];
        $produtos = $this->table->take(5)->where('tx_produto', 'like', "%$name%")->get();
        return $response->getBody()->write(json_encode($produtos));
    }

    public function findById(Request $request, Response $response, $args) {
        $id_produto = (int) $args['id_produto'];
        $produtos = $this->table->where('produto.id_produto', '=', $id_produto)->get();
        $produto = count($produtos) > 0 ? $produtos[0] : [];
        return $response->getBody()->write(json_encode($produto));
    }

    public function insert(Request $request, Response $response, $args) {
        $data = $request->getParsedBody();
        var_dump($data);
    }

}
