<?php

require 'classes/ProdutoController.php';

// Routes

/************ PRODUTO *************/

$app->get('/produto[/]', \App\ProdutoController::class . ':listAll');
$app->get('/produto/{name:[a-zA-Z]+}[/]', \App\ProdutoController::class . ':listByName');
$app->get('/produto/{id_produto:[0-9]+}[/]', \App\ProdutoController::class . ':findById');

$app->post('/produto[/]',\App\ProdutoController::class.":insert");

$app->put('/produto[/]',\App\ProdutoController::class.":update");

$app->delete('/produto[/]',\App\ProdutoController::class.":delete");

/************ PRODUTO SETOR *************/